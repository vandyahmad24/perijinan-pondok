<?php

namespace App\Http\Controllers;

use App\Perijinan;
use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Profile;
use DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Redirect;

class PengurusController extends Controller
{
    public function index()
    {
        $id = Auth::user()->id;
        $user = User::find($id);
        $profile = Profile::where('id',$user->profile_id)->first();
        return view ('pengurus.index',compact('user','profile'));
    }
    public function editData($id)
    {
        $province = DB::table('provinces')->get();
        $profiles = Profile::find($id);
        return view('pengurus/modal_edit',compact('profiles','province'));
    }
    public function postData(Request $request)
    {
        $profil = Profile::find($request->id);
        if($request->file('foto') == "")
        {
            $nama_file=$profil->foto;
        }
        else
        {
            $file = $request->file('foto');
            $nama_file = $request->fullname.".".$file->getClientOriginalExtension();
            $tujuan_upload = 'foto';
            $file->move($tujuan_upload,$nama_file);
        }
        
        $profil->fullname = $request->fullname;
        $profil->foto = $nama_file;
        $profil->jenis_kelamin = $request->jenis_kelamin;
        $profil->alamat = $request->alamat;
        $profil->provinsi = $request->provinsi;
        $profil->kabupaten = $request->kabupaten;
        $profil->updated_at = Carbon::now();
        $profil->save();

        $user = User::where('profile_id',$request->id)->first();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->level = 'pengurus';
        $user->updated_at = Carbon::now();
        $user->save();

        return redirect('/pengurus');
    }
    public function perijinan()
    {
        $perijinans = Perijinan::orderBy('created_at','desc')->get();
        return view('pengurus/perijinan',compact('perijinans'));
    }
    public function deletePerijinan($id)
    {
       $perijinan = Perijinan::find($id);
       $perijinan->delete();
       return redirect('/pengurus/perijinan');
    }
    public function editPerijinan($id)
    {
        $perijinan = Perijinan::find($id);
        return view('pengurus/modal_perijinan',compact('perijinan'));
    }
    public function putPerijinan(Request $request)
    {
        
        $perijinan = Perijinan::find($request->id);
        $perijinan->user_id = $request->user_id;
        $perijinan->tujuan = $request->tujuan;
        $perijinan->alasan = $request->alasan;
        $perijinan->mulai_ijin = $request->mulai_ijin;
        $perijinan->akhir_ijin = $request->akhir_ijin;
        $perijinan->no_hp = $request->no_hp;
        $perijinan->status = $request->status;
        if(is_null($request->catatan)){
            $catatan = '';
        }else{
            $catatan = $request->catatan;
        }
        $perijinan->catatan = $catatan;
        $perijinan->save();
        return redirect('/pengurus/perijinan');

    }
    public function sendWa($id)
    {
        $perijinan = Perijinan::find($id);
        $catatan = str_replace(" ", "%20", $perijinan->catatan);
        $newNumber = preg_replace('/^0?/', '62', $perijinan->no_hp);
        return redirect()->to("https://api.whatsapp.com/send?phone=".$newNumber."&text=".$catatan);
      
      
    }
}

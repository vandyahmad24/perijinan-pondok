<?php

namespace App\Http\Controllers;
use App\Profile;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class AdminController extends Controller
{
 

    public function index()
    {
        $profiles = Profile::paginate(5);
        $admin = User::where('level','admin')->count();
        $santri = User::where('level','santri')->count();
        $pengurus = User::where('level','pengurus')->count();
       
        return view ('admin.admin',compact('profiles','admin','santri','pengurus'));
    }
    public function DaftarSantri(Request $request)
    {
        $province = DB::table('provinces')->get();
        $profiles = Profile::paginate(15);
        return view ('admin.santri',compact('profiles','province'));
    }
    public function getKabupaten(Request $request)
    {
        $provinsi = DB::table('provinces')->where('name',$request->prov_id)->first();
        $kabupaten = DB::table('kabupaten')->where('province_id',$provinsi->id)->get();
        return response()->json([
            'status' => 'success',
            'data' => $kabupaten
        ]);

    }
    public function PostSantri(Request $request)
    {
        $nis = Profile::all()->last();
        $nisnow = $nis->nis += 1;
        $nisjadi = str_pad($nisnow, 4, '0', STR_PAD_LEFT);

        $profil = new Profile;
        $profil->nis = $nisjadi;
        $profil->fullname = $request->fullname;
        // menyimpan gambar
            $file = $request->file('foto');
            $nama_file = $request->fullname.".".$file->getClientOriginalExtension();
            $tujuan_upload = 'foto';
            $file->move($tujuan_upload,$nama_file);
        $profil->foto = $nama_file;
        $profil->jenis_kelamin = $request->jenis_kelamin;
        $profil->alamat = $request->alamat;
        $profil->provinsi = $request->provinsi;
        $profil->kabupaten = $request->kabupaten;
        $profil->status = 'pondok';
        $profil->save();

        $user = new User;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = Hash::make(123123);
        $user->level = $request->level;
        $user->profile_id = $profil->id;
        $user->created_at = Carbon::now();
        $user->updated_at = Carbon::now();
        $user->save();

        return redirect('admin/daftar-santri');
    }
    public function ShowSantri(Request $request,$id)
    {
        $province = DB::table('provinces')->get();
        $profiles = Profile::find($request->id);
        return view('admin/modal/edit_santri',compact('profiles','province'));
    }
    public function PutSantri(Request $request)
    {
        $profil = Profile::find($request->id);
        if($request->file('foto') == "")
    	{
    		$nama_file=$profil->foto;
    	}
    	else
    	{
            $file = $request->file('foto');
            $nama_file = $request->fullname.".".$file->getClientOriginalExtension();
            $tujuan_upload = 'foto';
            $file->move($tujuan_upload,$nama_file);
        }
        
        $profil->fullname = $request->fullname;
        $profil->foto = $nama_file;
        $profil->jenis_kelamin = $request->jenis_kelamin;
        $profil->alamat = $request->alamat;
        $profil->provinsi = $request->provinsi;
        $profil->kabupaten = $request->kabupaten;
        $profil->updated_at = Carbon::now();
        $profil->save();

        $user = User::where('profile_id',$request->id)->first();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->level = $request->level;
        $user->updated_at = Carbon::now();
        $user->save();

        return redirect('admin/daftar-santri');
    }
    public function DeleteSantri($id)
    {
        $user = User::where('profile_id',$id)->first();
        $user->delete();
        $profil = Profile::find($id);
        $profil->delete();
        return redirect('admin/daftar-santri');
    }
   
}

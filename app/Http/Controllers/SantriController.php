<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Profile;
use Auth;
use DB;
use Hash;
use Validator;
use Carbon\Carbon;
use App\Perijinan;

class SantriController extends Controller
{
     
        public function index()
        {
            $id = Auth::user()->id;
            $user = User::find($id);
            $profile = Profile::where('id',$user->profile_id)->first();
            return view ('santri.index', compact('user','profile'));
        }
        public function changePassword()
        {
            return view ('santri.change_password');
        }
        public function updatePassword(Request $request)
        {
            // custom validator
                Validator::extend('password', function ($attribute, $value, $parameters, $validator) {
                    return Hash::check($value, \Auth::user()->password);
                });
        
                // message for custom validation
                $messages = [
                    'password' => 'Invalid current password.',
                ];
        
                // validate form
                $validator = Validator::make(request()->all(), [
                    'current_password'      => 'required|password',
                    'password'              => 'required|min:6|confirmed',
                    'password_confirmation' => 'required',
        
                ], $messages);
        
                // if validation fails
                if ($validator->fails()) {
                    return redirect()
                        ->back()
                        ->withErrors($validator->errors());
                }
        
                // update password
                $user = User::find(Auth::id());
        
                $user->password = bcrypt(request('password'));
                $user->save();
        
                return redirect('/santri');
        }
        public function editData($id)
        {
            $province = DB::table('provinces')->get();
            $profiles = Profile::find($id);
            return view('santri/modal_edit',compact('profiles','province'));
        }
        public function putData(Request $request)
        {
            $profil = Profile::find($request->id);
            if($request->file('foto') == "")
            {
                $nama_file=$profil->foto;
            }
            else
            {
                $file = $request->file('foto');
                $nama_file = $request->fullname.".".$file->getClientOriginalExtension();
                $tujuan_upload = 'foto';
                $file->move($tujuan_upload,$nama_file);
            }
            
            $profil->fullname = $request->fullname;
            $profil->foto = $nama_file;
            $profil->jenis_kelamin = $request->jenis_kelamin;
            $profil->alamat = $request->alamat;
            $profil->provinsi = $request->provinsi;
            $profil->kabupaten = $request->kabupaten;
            $profil->updated_at = Carbon::now();
            $profil->save();

            $user = User::where('profile_id',$request->id)->first();
            $user->name = $request->name;
            $user->email = $request->email;
            $user->level = 'santri';
            $user->updated_at = Carbon::now();
            $user->save();

            return redirect('/santri');
        }
        public function perijinan()
        {
            $user_id = Auth::user()->id;
            $perijinan = Perijinan::where('user_id',$user_id)->orderBy('created_at','desc')->get();
            return view('santri/menu_perijinan',compact('perijinan'));
        }
        public function Createperijinan(Request $request)
        {
            $perijinan = new Perijinan;
            $perijinan->user_id = $request->id;
            $perijinan->tujuan = $request->tujuan;
            $perijinan->alasan = $request->alasan;
            $perijinan->mulai_ijin = $request->mulai_ijin;
            $perijinan->akhir_ijin = $request->akhir_ijin;
            $perijinan->no_hp = $request->no_hp;
            $perijinan->catatan = '';
            $perijinan->status = 'menunggu';
            $perijinan->save();
            return redirect('/santri/perijinan');
            
        }
        public function editPerijinan($id)
        {
            $perijinan = Perijinan::find($id);
            return view('santri/modal_edit_perijinan',compact('perijinan'));
        }
        public function putPerijinan(Request $request)
        {
            $perijinan = Perijinan::find($request->perijinan_id);
            $perijinan->user_id = $request->id;
            $perijinan->tujuan = $request->tujuan;
            $perijinan->alasan = $request->alasan;
            $perijinan->mulai_ijin = $request->mulai_ijin;
            $perijinan->akhir_ijin = $request->akhir_ijin;
            $perijinan->no_hp = $request->no_hp;
            $perijinan->catatan = '';
            $perijinan->status = 'menunggu';
            $perijinan->save();
            return redirect('/santri/perijinan');

        }

        
}

@extends('layouts.admin')
@section('title','Profil Santri')

@section('content')

 <!-- Page Content -->
  <div class="page-content">
                <!-- Page Header -->
                <div class="page-header">
                    <div class="search-form">
                        <form action="#" method="GET">
                            <div class="input-group">
                                <input type="text" name="search" class="form-control search-input" placeholder="Type something...">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" id="close-search" type="button"><i class="icon-close"></i></button>
                                </span>
                            </div>
                        </form>
                    </div>
                    <nav class="navbar navbar-default">
                        <div class="container-fluid">
                            <!-- Brand and toggle get grouped for better mobile display -->
                            <div class="navbar-header">
                                <div class="logo-sm">
                                    <a href="javascript:void(0)" id="sidebar-toggle-button"><i class="fa fa-bars"></i></a>
                                    <a class="logo-box" href="#"><span>Ponpes</span></a>
                                </div>
                                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                                    <i class="fa fa-angle-down"></i>
                                </button>
                            </div>
                        
                            <!-- Collect the nav links, forms, and other content for toggling -->
                        
                            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                                <ul class="nav navbar-nav navbar-right">
                                    <li class="dropdown user-dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> Welcome,    {{Auth::user()->name}} </a>
                                        <ul class="dropdown-menu">
                                         
                                            <li><a href="{{route('change-password')}}">Change Password</a></li>
                                            <li><a href="{{route('keluar')}}">Log Out</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </div><!-- /.navbar-collapse -->
                        </div><!-- /.container-fluid -->
                    </nav>
                </div><!-- /Page Header -->
                <!-- Page Inner -->
                <div class="page-inner">
                    <div class="page-title">
                        <h3 class="breadcrumb-header">Dashboard</h3>
                    </div>
                    <div id="main-wrapper">
                    
                        <div class="row">
                            <div class="col-lg-8 col-md-12"></div>
                                <div class="panel panel-white">
                                    <a href="#" class="btn btn-success" title="Tambah Perijinan" data-toggle="modal" data-target="#tambahperijinan"> Buat Perijinan </a>
                                   <div class="panel-body">
                                    @if(count($perijinan) > 0)
                                    <table class="table">
                                        <thead>
                                          <tr>
                                            <th scope="col">No</th>
                                            <th scope="col">Tujuan</th>
                                            <th scope="col">Alasan</th>
                                            <th scope="col">Tanggal Ijin</th>
                                            <th scope="col">Tanggal Kembali</th>
                                            <th scope="col">No Hp</th>
                                            <th scope="col">Status</th>
                                            <th scope="col">Catatan</th>
                                            <th scope="col">Aksi</th>
                                          </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($perijinan as $item)
                                          <tr>
                                          <th scope="row">{{$loop->iteration}}</th>
                                            <td>{{$item->tujuan}}</td>
                                            <td>{{$item->alasan}}</td>
                                            <td>{{$item->mulai_ijin}}</td>
                                            <td>{{$item->akhir_ijin}}</td>
                                            <td>{{$item->no_hp}}</td>
                                            <td>
                                                @if($item->status =='menunggu')
                                                <span class="label label-warning ">Menunggu Konfirmasi</span>
                                                @elseif($item->status =='setuju')
                                                <span class="label label-success ">Perijinan di Setujui</span>
                                                @else
                                                <span class="label label-danger ">Perijinan di Tolak</span>
                                                @endif
                                            </td>
                                            <td>{{$item->catatan}}</td>
                                            <td>
                                                @if($item->status == 'menunggu')
                                                <a href="#" value="{{ action('SantriController@editPerijinan',['id'=>$item->id]) }}" class="btn btn-success editModal" title="Edit Perijinan" data-toggle="modal" data-target="#editModal"> Edit</a>
                                                <a href="" class="btn btn-danger"> Hapus</a>
                                                @else
                                                <button disabled="disabled" class="btn btn-success">Edit</button>
                                                <button disabled="disabled" class="btn btn-danger">Hapus</button>
                                                @endif
                                            </td>
                                        </tr>
                                        @endforeach
                                        </tbody>
                                      </table>      
                                    @else
                                      <h3> Kamu Belum Membuat Perijinan</h3>
                                      @endif
                                    </div>
                                </div>
                            </div>
                            
                        </div><!-- Row -->

                        {{-- Modal --}}
                        <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="editModal" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                              <div class="modal-content">
                                <div class="modal-header">
                                  <h5 class="modal-title" id="exampleModalLabel">Edit User</h5>
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                  </button>
                                </div>
                                <div class="modal-body" id="modalEditContent">
                                Mohon Tunggu . . .
                                </div>
                                <div class="modal-footer">
                                   <br>
                                  </div>
                              </div>
                            </div>
                          </div>

                        <div class="modal fade" id="tambahperijinan" tabindex="-1" role="dialog" aria-labelledby="tambahperijinan" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                              <div class="modal-content">
                                <div class="modal-header">
                                  <h5 class="modal-title" id="exampleModalLabel">Buat Perijinan</h5>
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                  </button>
                                </div>
                                <div class="modal-body">
                                <form action="{{route('create-perijinan')}}" method="POST" enctype="multipart/form-data">
                                        {{ csrf_field() }}
                                    <input type="hidden" name="id" value="{{Auth::user()->id}}">
                                    <div class="form-group">
                                        <label for="name">Tujuan Ijin</label>
                                        <input type="text" class="form-control" value="" name="tujuan" id="tujuan" placeholder="Tujuan Ijin">
                                    </div>
                                    <div class="form-group">
                                        <label for="name">Tanggal Ijin</label>
                                        <input type="date" name="mulai_ijin" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label for="name">Tanggal Selesai</label>
                                        <input type="date" name="akhir_ijin" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label for="name">Alasan Ijin</label>
                                        <textarea class="form-control" name="alasan" id="exampleFormControlTextarea1" rows="3"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label for="name">Nomer Hp</label>
                                        <input type="text" class="form-control" value="" name="no_hp" id="no_hp" placeholder="Nomer Hp">
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                    <button type="submit" class="btn btn-primary">Buat Ijin</button>
                                  </div>
                              </div>
                            </div>
                          </div>
                        
                    </div><!-- Main Wrapper -->
                    <div class="page-footer">
                        <p>Made with <i class="fa fa-heart"></i> Zuhrof Karimah Hamida</p>
                    </div>
                </div><!-- /Page Inner -->
                
            </div><!-- /Page Content -->
            <script>
            $(".editModal").on("click",function(){
                $('#modalEditContent').load($(this).attr('value'));
            });
            </script>
@endsection